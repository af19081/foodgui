import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FoodGUI {
    private JPanel root;
    private JButton tempuraButton;
    private JButton ramenButton;
    private JButton udonButton;
    private JTextPane orderList;
    private JButton karaageButton;
    private JButton gyozaButton;
    private JButton yakisobaButton;
    private JButton checkOutButton;
    private JLabel total;
    private JLabel price;

    int totalPrice = 0;

    void order(String food) {
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order " + food + "?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);
        if (confirmation == 0) {
            String temp = orderList.getText();
            if(temp==""){
                orderList.setText(food);
            }
            else {
                orderList.setText(temp + "\n" + food);
            }
            //100円加算
            totalPrice += 100;
            price.setText(String.valueOf(totalPrice) + " yen");
            //
            JOptionPane.showMessageDialog(null,
                    "Thank you for ordering " + food + "! It will be served as soon as possible.");
        }
    }


    public FoodGUI() {
        tempuraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tempura");
            }
        });
        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Ramen");
            }
        });
        udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Udon");
            }
        });
        karaageButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Karaage");
            }
        });
        yakisobaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Yakisoba");
            }
        });
        gyozaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Gyoza");
            }
        });
        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "Would you like to checkout?",
                        "Checkout Confirmation",
                        JOptionPane.YES_NO_OPTION);
                if(confirmation == 0) {
                    JOptionPane.showMessageDialog(null,
                            "Thank you. The total price is " + totalPrice + " yen.");
                    //金額リセット
                    totalPrice = 0;
                    price.setText(String.valueOf(totalPrice) + " yen");
                    //
                    orderList.setText("");
                }
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodGUI");
        frame.setContentPane(new FoodGUI().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
